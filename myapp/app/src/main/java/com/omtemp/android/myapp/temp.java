package com.omtemp.android.myapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import static java.lang.Math.sqrt;

public class temp extends AppCompatActivity {

    private DatabaseReference myRef,myRef1,myref2;
    private ListView lv;
    private String value1,value2,valuename,valuephno,value3,val1,val2,val3,valname,valphno,valueloc;
    private int numx1,numy1,numx2,numy2;
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseauth;
    public Button btn;
    List<String> mylist= new ArrayList<String>();
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);
        btn = (Button) findViewById(R.id.rescued);
        lv=(ListView) findViewById(R.id.list);

        //mref=new Firebase("https://temp-280f9.firebaseio.com/Users in Emergency");

        myRef = database.getReference("All users");
myRef1=database.getReference("emergency");
        myref2=database.getReference("emergency");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid=user.getUid();

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()) {
                    // This method is called once with the initial value and again
                    // whenever data at this location is updated.
                    //  values = dataSnapshot1.getKey();

                    if(uid.equals(dataSnapshot1.getKey())){
                        value1=dataSnapshot1.child("x val").getValue().toString();
                        value2=dataSnapshot1.child("y val").getValue().toString();
                        valuename=dataSnapshot1.child("name").getValue().toString();
                        valuephno=dataSnapshot1.child("phno").getValue().toString();
                        valueloc=dataSnapshot1.child("location").getValue().toString();
                        try {
                            numx1 = Integer.parseInt(value1);
                        } catch(NumberFormatException nfe) {
                            System.out.println("Could not parse ");
                        }
                        try {
                            numy1 = Integer.parseInt(value2);
                        } catch(NumberFormatException nfe) {
                            System.out.println("Could not parse ");
                        }
break;
                    }}
                   // value3=dataSnapshot1.getKey();

                    //Log.d(TAG, "Value is: " + value);
                for(DataSnapshot dataSnapshot2:dataSnapshot.getChildren()) {
                    val1=dataSnapshot2.child("x val").getValue().toString();
                    val2=dataSnapshot2.child("y val").getValue().toString();
                    valname=dataSnapshot2.child("name").getValue().toString();
                    valphno=dataSnapshot2.child("phno").getValue().toString();
                    val3=dataSnapshot2.getKey();
                    try {
                        numx2 = Integer.parseInt(val1);
                    } catch(NumberFormatException nfe) {
                        System.out.println("Could not parse ");
                    }
                    try {
                        numy2 = Integer.parseInt(val2);
                    } catch(NumberFormatException nfe) {
                        System.out.println("Could not parse ");
                    }

                    if(sqrt((numx1-numx2)*(numx1-numx2)+(numy1-numy2)*(numy1-numy2))<100 )
                    {
                        if(uid.equals(val3)) {
                        }
                        else {
                            mylist.add("  name=  " + valname + "                                phone number=  " + valphno);
                            databaseReference = FirebaseDatabase.getInstance().getReference("emergency").child(val3);
                            databaseReference.child("x val").setValue(val1);
                            databaseReference.child("y val").setValue(val2);
                            databaseReference.child("name").setValue(valname);
                            databaseReference.child("phno").setValue(valphno);
//details of called user
                            databaseReference.child("uidcu").setValue(uid);
                            databaseReference.child("x valcu").setValue(value1);
                            databaseReference.child("y valcu").setValue(value2);
                            databaseReference.child("namecu").setValue(valuename);
                            databaseReference.child("phnocu").setValue(valuephno);
                            databaseReference.child("location").setValue(valueloc);
                        }
                    }

                }



                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        getBaseContext(),
                        android.R.layout.simple_list_item_1,
                        mylist );

                lv.setAdapter(arrayAdapter);





            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                //Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

btn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid=user.getUid();

        myref2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot dataSnapshot3:dataSnapshot.getChildren()) {
                    String keycu = dataSnapshot3.getKey();
                    String key=dataSnapshot3.child("uidcu").getValue().toString();
                    if(uid.equals(key)) {

                       // Toast.makeText(temp.this,"",Toast.LENGTH_LONG).show();
                         databaseReference = FirebaseDatabase.getInstance().getReference("emergency").child(keycu);
                        databaseReference.removeValue();


                    }
                    }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        Intent i=new Intent(temp.this,choice.class);
        startActivity(i);
        //FirebaseAuth.getInstance().signOut();


    }



});
        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                NotificationCompat.Builder builder =
                        (NotificationCompat.Builder) new NotificationCompat.Builder(getBaseContext())
                                .setSmallIcon(R.drawable.dog)
                                .setContentTitle("ALERT")
                                .setContentText("Nearby Users Have been Informed");

                Intent notificationIntent = new Intent(getBaseContext(), temp.class);
                PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(), 0, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                builder.setContentIntent(contentIntent);

                // Add as notification
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                manager.notify(0, builder.build());


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





    }






}