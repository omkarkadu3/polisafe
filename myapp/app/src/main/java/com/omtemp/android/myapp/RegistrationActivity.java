package com.omtemp.android.myapp;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistrationActivity extends AppCompatActivity {
    private EditText email;
    private EditText password;
    private FirebaseAuth firebaseauth;
    public static final String KEY_ROWID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_PHONE = "phone";
    private static final String DATABASE_NAME = "MyDB";
    private static final String DATABASE_TABLE = "contacts";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE =
            "create table contacts (_id integer primary key " +
                    "autoincrement, " + "name text not null, " +
                    "email text not null, " +
                    "phone int not null);";

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;

    public RegistrationActivity(Context ctx)
    {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        email= (EditText) findViewById(R.id.txtemailReg);
        password= (EditText) findViewById(R.id.txtpasswordReg);
        firebaseauth=FirebaseAuth.getInstance();

    }


    public void btnRegisterUser_click(View v)
    {
        final ProgressDialog progressDialog=ProgressDialog.show(RegistrationActivity.this,"waitRegister..","processing.....",true);
        (firebaseauth.createUserWithEmailAndPassword(email.getText().toString(),password.getText().toString())).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
progressDialog.dismiss();

                if(task.isSuccessful())
                {
                    //Toast.makeText(RegistrationActivity.this,"Registered",Toast.LENGTH_LONG).show();
                    Intent i=new Intent(RegistrationActivity.this,Details.class);
                    startActivity(i);
                }
                else{
                    Log.e("error",task.getException().toString());
                    Toast.makeText(RegistrationActivity.this,task.getException().getMessage(),Toast.LENGTH_LONG).show();
                }
            }
        });


    }


    private static class DatabaseHelper
            extends SQLiteOpenHelper
    {
        DatabaseHelper(Context context)
        {
            super(context, DATABASE_NAME,
                    null, DATABASE_VERSION);
            //SQLiteOpenHelper (Context context,String name, SQLiteDatabase.CursorFactory factory, int version)
            //SQLiteDatabase.CursorFactory is null by default
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            try
            {
                db.execSQL(DATABASE_CREATE);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db,
                              int oldVersion, int newVersion)
        {
            //Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
            //+ newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS contacts");

            onCreate(db);
        }
    }

    //---opens the database---
    public RegistrationActivity open() throws SQLException
    {
        db = DBHelper.getWritableDatabase();
        return this;
    }

    //---closes the database---
    public void close()
    {
        DBHelper.close();
    }

    //---insert a contact into the database---
    public long insertContact(String name, String email,
                              String phone)
    {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_NAME, name);
        initialValues.put(KEY_EMAIL, email);
        initialValues.put(KEY_PHONE, phone);
        return db.insert(DATABASE_TABLE, null, initialValues);
        // 2nd parameter -  null indicates all column values.
    }

    //---deletes a particular contact---
    public boolean deleteContact(long rowId)
    {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId,
                null)	> 0;
        // relational operator returns true / false
    }

    //---retrieves all the contacts---
    public Cursor getAllContacts()
    {
        return db.query(DATABASE_TABLE, new String[]
                        {KEY_ROWID, KEY_NAME,KEY_EMAIL,KEY_PHONE},
                null, null, null, null, null);
    }

    //---retrieves a particular contact---
    public Cursor getContact(long rowId)
    {
        Cursor mCursor =
                db.query(DATABASE_TABLE, new String[] {KEY_ROWID,
                                KEY_NAME, KEY_EMAIL,KEY_PHONE}, KEY_ROWID + "=" + rowId,
                        null,null, null, null);

        return mCursor;
    }

    //---updates a contact---
    public boolean updateContact(long rowId, String name,
                                 String email, String phone)
    {
        ContentValues args = new ContentValues();
        args.put(KEY_NAME, name);
        args.put(KEY_EMAIL, email);
        args.put(KEY_PHONE, phone);
        return db.update(DATABASE_TABLE, args, KEY_ROWID +
                "="	+ rowId, null) > 0;
    }
}



