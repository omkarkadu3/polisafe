package com.omtemp.android.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user!=null) {
            // User is signed in.
            Toast.makeText(MainActivity.this,"Already Logged In",Toast.LENGTH_LONG).show();
            Intent i=new Intent(MainActivity.this,Activity.class);
            startActivity(i);
        } else {
            // No user is signed in.
            Toast.makeText(MainActivity.this,"Not logged In",Toast.LENGTH_LONG).show();
        }
    }

    public void btnRegistration_click(View v)
    {
        Intent i=new Intent(MainActivity.this,RegistrationActivity.class);
        startActivity(i);

    }

    public void btnLogin_click(View v)
    {
        Intent i=new Intent(MainActivity.this,LoginActivity.class);
        startActivity(i);
    }
}

