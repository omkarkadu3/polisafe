package com.omtemp.android.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class choice extends AppCompatActivity {
    private Button btn,btn1;
    private DatabaseReference myRef,myRef1;
    //private int om=0;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choice);

        btn = (Button) findViewById(R.id.alert);
       // btn1 = (Button) findViewById(R.id.notify);
        myRef1=database.getReference("emergency");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid=user.getUid();

        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dataSnapshot2:dataSnapshot.getChildren()) {
                    if(uid.equals(dataSnapshot2.getKey())) {
                        Intent i=new Intent(choice.this,tempnew.class);
                        startActivity(i);
                    } else{

                    }


            }}

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



    }
    public void alert(View v) {
        Intent i=new Intent(choice.this,temp.class);
        startActivity(i);

    }

    public void logout(View v) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseAuth.getInstance().signOut();
            Intent i=new Intent(choice.this,MainActivity.class);
            startActivity(i);


    }


    }




