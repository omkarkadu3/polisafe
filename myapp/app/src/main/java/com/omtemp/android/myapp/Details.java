package com.omtemp.android.myapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Details extends AppCompatActivity {

    private EditText name;
    private EditText phno;
    private FirebaseAuth firebaseauth;
    private DatabaseReference databaseReference;
    public String namest,phnost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        name= (EditText) findViewById(R.id.name);
        phno=(EditText) findViewById(R.id.phno);
        Button button1 = (Button) findViewById(R.id.button);






button1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid=user.getUid();
namest=name.getText().toString();
        phnost=phno.getText().toString();

        databaseReference= FirebaseDatabase.getInstance().getReference("All users").child(uid);
        databaseReference.child("name").setValue(namest);
        databaseReference.child("phno").setValue(phnost);

        Toast.makeText(Details.this,"Registered",Toast.LENGTH_LONG).show();
        Intent i=new Intent(Details.this,LoginActivity.class);
        startActivity(i);

    }
});

    }}

