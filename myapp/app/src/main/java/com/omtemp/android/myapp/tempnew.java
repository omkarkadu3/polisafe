package com.omtemp.android.myapp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class tempnew extends AppCompatActivity {
    private DatabaseReference databaseReference;
    private FirebaseAuth firebaseauth;
    private DatabaseReference myRef,myRef1;
    private String value1,value2,value3,val1,val2,val3,valuename,valuephno,valloc;
    private TextView name,phno,xval,yval,loc;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tempnew);
        myRef1=database.getReference("emergency");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid=user.getUid();

        name= (TextView) findViewById(R.id.textname);
        phno= (TextView) findViewById(R.id.textphno);
        xval= (TextView) findViewById(R.id.textx);
        yval= (TextView) findViewById(R.id.texty);
        loc= (TextView) findViewById(R.id.location);

        myRef1.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for(DataSnapshot dataSnapshot2:dataSnapshot.getChildren()) {
                    if(uid.equals(dataSnapshot2.getKey()) && dataSnapshot2.child("uidcu").getValue().toString()!="") {
                        val1 = dataSnapshot2.child("x valcu").getValue().toString();
                        val2 = dataSnapshot2.child("y valcu").getValue().toString();
                        valuename = dataSnapshot2.child("namecu").getValue().toString();
                        valuephno = dataSnapshot2.child("phnocu").getValue().toString();
                        valloc = dataSnapshot2.child("location").getValue().toString();

                        name.setText(valuename);
                        phno.setText(valuephno);
                        xval.setText(val1);
                        yval.setText(val2);
                        loc.setText(valloc);


                        NotificationCompat.Builder builder =
                                (NotificationCompat.Builder) new NotificationCompat.Builder(getBaseContext())
                                        .setSmallIcon(R.drawable.dog)
                                        .setContentTitle("ALERT")
                                        .setContentText("  name=" + valuename + "    Contact="+valuephno);

                        Intent notificationIntent = new Intent(getBaseContext(), tempnew.class);
                        PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(), 0, notificationIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
                        builder.setContentIntent(contentIntent);

                        // Add as notification
                        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        manager.notify(0, builder.build());
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
    public void logout(View v) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        FirebaseAuth.getInstance().signOut();
        Intent i=new Intent(tempnew.this,MainActivity.class);
        startActivity(i);


    }


}
